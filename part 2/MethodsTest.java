public class MethodsTest {
	public static void main(String[] args) {
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(3, 5.5);
		int z = methodNoInputReturnInt();
		System.out.println("z = "+z);
		double squareRootVariable = sumSquareRoot(9, 5);
		System.out.println(squareRootVariable);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
		
	}
	
	public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike) {
		System.out.println("Inside the method one input no return");
		youCanCallThisWhateverYouLike = youCanCallThisWhateverYouLike - 5;
		System.out.println(youCanCallThisWhateverYouLike);
		
	}
	
	public static void methodTwoInputNoReturn(int a, double b) {
		System.out.println(a);
		System.out.println(b);
	}
	
	public static int methodNoInputReturnInt() {
		return 5;
		
	}
	
	public static double sumSquareRoot(int a, int b) {
		int ab = a + b;
		double squareRootOfAb = Math.sqrt(ab);
		return squareRootOfAb;
	}
	
}