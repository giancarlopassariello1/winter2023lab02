public class Calculator {
	
	public double add (double a, double b) {
		double sum = a + b;
		return sum;	
	}
	
	public double subtract (double a, double b) {
		double sum = a - b;
		return sum;
	}
	
	public static double multiply (double a, double b) {
		double sum = a * b;
		return sum;
	}
	
	public static double divide (double a, double b) {
		double sum = a / b;
		return sum;
	}

}