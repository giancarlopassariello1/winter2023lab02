import java.util.Scanner;
public class PartThree {
	
	public static void main (String[] args) {
		Scanner r = new Scanner(System.in);
		System.out.println("Enter your first value!");
		double valueOne = r.nextDouble();
		
		System.out.println("Enter your second value!");
		double valueTwo = r.nextDouble();
		
		Calculator c = new Calculator();
	
		System.out.println("Addition Result : "+c.add(valueOne, valueTwo));
		System.out.println("Subtraction Result : "+c.subtract(valueOne, valueTwo));
		System.out.println("Multiplication Result : "+Calculator.multiply(valueOne, valueTwo));
		System.out.println("Division Result : "+Calculator.divide(valueOne, valueTwo));
	}
	
}